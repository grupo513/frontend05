import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import Cita from "../../../modelos/Cita";
import ApiBack from "../../../utilidades/dominios/ApiBack";
import ServicioPrivado from "../../../servicios/ServicioPrivado";
import { obtenerFechaLocal, obtenerHora, } from "../../../utilidades/funciones/FormatoFecha";


export const CitaAgendadaCrear = () => {
  // Variables
  const [arregloCitas, setArregloCitas] = useState<Cita[]>([]);
  // ************************************************************************


  // Función para obtener perfiles
  const obtenerCitas = async () => {
    const resultado = await ServicioPrivado.peticionGET(ApiBack.CITAS_OBTENER);
    setArregloCitas(resultado);
    return resultado;
  };
  // ************************************************************************


 
  

  // Hook de react que se usa cuando se renderiza o pinta la página (vista)
  useEffect(() => {
    obtenerCitas();
  }, []);
  // ************************************************************************

  
  return (
    <main id="main" className="main">
      {/* Navegación estilo breadcrumb: Inicio */}
      <div className="pagetitle">
        <h1>Citas Disponibles</h1>
        <nav>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="index.html">Inicio</a>
            </li>
            <li className="breadcrumb-item active">Listado de Citas</li>
          </ol>
        </nav>
      </div>
      {/* Navegación estilo breadcrumb: Fin */}

      {/* Ejemplo de una tabla para presentación de datos: Inicio */}
      <div className="col-lg-12">
        <div className="card">
          <div className="card-body">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th className="text-center" style={{ width: "10%" }}>
                    Nro
                  </th>
                  <th style={{ width: "20%" }}>Fecha cita</th>
                  <th style={{ width: "55%" }}>Médico</th>
                  <th className="text-center" style={{ width: "15%" }}>
                    Agendar cita
                  </th>
                </tr>
              </thead>
              <tbody>
                {arregloCitas.map((miUsu, indice) => (
                  <tr key={indice}>
                    <td className="text-center">{indice + 1}</td>
                    <td>
                      {obtenerFechaLocal(miUsu.fechaCita)}
                      <span> - </span>
                      <small>{obtenerHora(miUsu.fechaCita)}</small>
                    </td>
                    <td>
                      <small>{miUsu.codMedico.nombreUsuario}</small>
                    </td>
                    <td className="text-center">
                    <button className="btn btn-primary">  <Link to={"/dashboard/agendar" }>
                        <i
                          className="fa-regular fa-calendar-plus"
                        ></i>
                      </Link>
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      {/* Ejemplo de una tabla para presentación de datos: Fin */}
    </main>
  );
};