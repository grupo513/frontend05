import { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";

import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Cita from "../../../modelos/Cita";
import { ToastContainer } from "react-toastify";
import CitaAgendada from "../../../modelos/CitaAgendada";
import Usuario from "../../../modelos/Usuario"; 
import ApiBack from "../../../utilidades/dominios/ApiBack";
import ServicioPrivado from "../../../servicios/ServicioPrivado";
import { useFormulario } from "../../../utilidades/misHooks/useFormulario";
import { MensajeToastify } from "../../../utilidades/funciones/MensajeToastify";


export const CitaAgendadaActual = () => {
  // Variables
  const fechaVacia = new Date();
  const usuarioVacio = new Usuario(
    "",
    "",
    "",
    "",
    fechaVacia,
    1,
    "",
    "",
    new Perfil("", "", 1)
  );




  let { codigo } = useParams();
  type formaHtml = React.FormEvent<HTMLFormElement>;
  const [enProceso, setEnProceso] = useState<boolean>(false);
  const [todoListo, setTodoListo] = useState<boolean>(false);
  let cargaFinalizada = todoListo !== undefined;
  let { codPaciente,codCita, dobleEnlace, objeto } =
    useFormulario<CitaAgendada>(new CitaAgendada("",new Usuario("","", "", "", new Date(), 1, "", "",new Cita( "", "", "", new Date(), 1 ) ));
  // *******************************************************************
  // Consultar datos del perfil a modificar
  // *******************************************************************
  const obtenerUnPaciente = async () => {
    const urlCargarUnPaciente = ApiBack.CITAS_AGENDADAS_OBTENER_UNO + "/" + codigo;
    const pacienteRecibido = await ServicioPrivado.peticionGET(urlCargarUnPaciente);
    objeto._id = pacienteRecibido._id;
    objeto.codPaciente = pacienteRecibido.codPaciente;
    objeto.codCita = pacienteRecibido.codCita;
    if (pacienteRecibido) {
      setTodoListo(true);
    }
  };
  // *******************************************************************

  const enviarFormulario = async (fh: formaHtml) => {
    fh.preventDefault();
    setEnProceso(true);
    const fromulario = fh.currentTarget;

    fromulario.classList.add("was-validated");
    if (fromulario.checkValidity() === false) {
      fh.preventDefault();
      fh.stopPropagation();
    } else {
      const urlActualizar = ApiBack.CITAS_AGENDADAS_ACTUALIZAR + "/" + objeto._id;
      const resultado = await ServicioPrivado.peticionPUT(
        urlActualizar,
        objeto
      );

      if (resultado.nuevo) {
        setEnProceso(false);
        MensajeToastify("success", "Paciente actualizado correctamente", 6000);
      } else {
        MensajeToastify(
          "error",
          "No se puede actualizar el paciente. Es posible que el nombre ya exista en la base de datos",
          6000
        );
      }
    }
  };

  useEffect(() => {
    obtenerUnPaciente();
  }, []);

  return (
    <main id="main" className="main">
      <div>
        {/* Navegación estilo breadcrumb: Inicio */}
        <div className="pagetitle">
          <h1>Pacientes</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/dashboard">Inicio</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to="/dashboard/admprofile">
                  Administración de pacientes
                </Link>
              </li>
              <li className="breadcrumb-item active">Actualizar</li>
            </ol>
          </nav>
        </div>
        {/* Navegación estilo breadcrumb: Fin */}
        {/* Ejemplo de formulario: Inicio */}
        <div className="col-lg-12">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">Formulario de edición</h5>
              {cargaFinalizada ? (
                <Form
                  noValidate
                  validated={enProceso}
                  onSubmit={enviarFormulario}
                >
                  <Form.Group
                    as={Row}
                    className="mb-3"
                    controlId="codPaciente"
                  >
                    <Form.Label column sm={2}>
                      Nombre perfil
                    </Form.Label>
                    <Col sm={10}>
                      <Form.Control
                        required
                        type="text"
                        name="codPaciente"
                        className="form-control"
                        value={codPaciente}
                        onChange={dobleEnlace}
                      />
                      <Form.Control.Feedback type="invalid">
                        Nombre del perfil es obligatorio
                      </Form.Control.Feedback>
                    </Col>
                  </Form.Group>

                  <Form.Group
                    as={Row}
                    className="mb-3"
                    controlId="codCita"
                  >
                    <Form.Label column sm={2}>
                      Estado perfil
                    </Form.Label>
                    <Col sm={10}>
                      <Form.Select
                        required
                        name="codCita"
                        value={codCita}
                        onChange={dobleEnlace}
                      >
                        <option value={1}>Activo</option>
                        <option value={2}>Inactivo</option>
                      </Form.Select>
                      <Form.Control.Feedback type="invalid">
                        Seleccione el estado del perfil
                      </Form.Control.Feedback>
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} className="mb-3">
                    <Col sm={{ span: 10, offset: 2 }}>
                      <Button type="submit" className="btn btn-sm">
                        Actualizar perfil
                      </Button>
                    </Col>
                  </Form.Group>
                </Form>
              ) : (
                <div>Cargando información para la edición</div>
              )}
            </div>
          </div>
        </div>
        {/* Ejemplo de formulario: Fin */}

        {/* Requerido para presentar los mensajes Toast: Inicio */}
        <ToastContainer />
        {/* Requerido para presentar los mensajes Toast: Fin */}
      </div>
    </main>
  );
};
