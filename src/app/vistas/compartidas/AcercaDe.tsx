import fotoTripulante from "../../../assets/image/acercade.png";
import fotoAndres from "../../../assets/image/andres.jpeg"
import fotoDagoberto from "../../../assets/image/dagoberto.jpg"

export const AcercaDe = () => {
  return (
    <main id="main" className="main">
      <div className="pagetitle">
        <h1>Desarrolladores</h1>
        <nav>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="index.html">Inicio</a>
            </li>
            <li className="breadcrumb-item active">Listado de tripulantes</li>
          </ol>
        </nav>
      </div>

      <div className="mt-4">
        <div className="card-group">
          <div className="card">
            <img src={fotoAndres} className="" alt="..." />
            <div className="card-body">
              <h5 className="card-title">Andres Javier Cuadros Sanabria</h5>
              <p className="card-text">Developer</p>
              <p className="card-text">
                <small className="text-muted">Ingeniero de sistemas UIS</small>
              </p>
            </div>
          </div>

          <div className="card">
            <img src={fotoDagoberto} className="" alt="..." />
            <div className="card-body">
              <h5 className="card-title">Dagoberto Sanchez Delgadello</h5>
              <p className="card-text">Developer</p>
              <p className="card-text">
                <small className="text-muted">Ingeniero de pertróleos UIS</small>
              </p>
            </div>
          </div>

          <div className="card">
            <img src={fotoTripulante} className="card-img-top" alt="..." />
            <div className="card-body">
              <h5 className="card-title">Daniel Antonio Acevedo Vega</h5>
              <p className="card-text">Developer</p>
              <p className="card-text">
                <small className="text-muted"></small>
              </p>
            </div>
          </div>

          <div className="card">
            <img src={fotoTripulante} className="card-img-top" alt="..." />
            <div className="card-body">
              <h5 className="card-title">Daniel Antonio Cárdenas Villamizar</h5>
              <p className="card-text">Developer</p>
              <p className="card-text">
                <small className="text-muted"></small>
              </p>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};