//admin
const opcionesAdmin = [
  { nombre: "Desarrolladores", icono: "bi bi-grid", ruta: "/dashboard/about", hijos: [], },
  { nombre: "Perfiles", icono: "bi bi-clipboard-data", ruta: "", hijos: [
      { nombre: "Listado", icono: "bi bi-circle", ruta: "/dashboard/listprofiles", },
      { nombre: "Nuevo", icono: "bi bi-circle", ruta: "/dashboard/addprofile" },
      { nombre: "Administración", icono: "bi bi-circle", ruta: "/dashboard/admprofile", },
    ]
  },
  { nombre: "Usuarios", icono: "bi bi-person-lines-fill", ruta: "", hijos: [
    { nombre: "Listado", icono: "bi bi-circle", ruta: "/dashboard/listusers", },
    { nombre: "Nuevo", icono: "bi bi-circle", ruta: "/dashboard/adduser" },
    { nombre: "Administración", icono: "bi bi-circle", ruta: "/dashboard/admuser", },
    ]
  },
  { nombre: "Citas", icono: "bi bi-calendar", ruta: "", hijos: [
    { nombre: "Listado", icono: "bi bi-circle", ruta: "/dashboard/listma", },
    { nombre: "Nuevo", icono: "bi bi-circle", ruta: "/dashboard/addma" },
    { nombre: "Administración", icono: "bi bi-circle", ruta: "/dashboard/admma", },
    ]
  },
  { nombre: "CitasAgendadas", icono: "bi bi-calendar", ruta: "", hijos: [
    { nombre: "Listado", icono: "bi bi-circle", ruta: "/dashboard/citaAgendada", },
    { nombre: "Nuevo", icono: "bi bi-circle", ruta: "/dashboard/citaAgendadaCrear" },
    { nombre: "Administración", icono: "bi bi-circle", ruta: "/dashboard/admCitaAgendada", },
    ]
  }
];

// *********************************************************************************
//Medico
const opcionesMedico = [
  { nombre: "Desarrolladores", icono: "bi bi-grid", ruta: "/dashboard/about", hijos: [], },
  { nombre: "Perfiles", icono: "bi bi-clipboard-data", ruta: "", hijos: [
      { nombre: "Listado", icono: "bi bi-circle", ruta: "/dashboard/listprofiles", },
    ]
  },
  { nombre: "Usuarios", icono: "bi bi-person-lines-fill", ruta: "", hijos: [
    { nombre: "Listado", icono: "bi bi-circle", ruta: "/dashboard/listusers", },
    { nombre: "Nuevo", icono: "bi bi-circle", ruta: "/dashboard/adduser" },
    ]
  },
  { nombre: "Citas", icono: "bi bi-calendar", ruta: "", hijos: [
    { nombre: "Listado", icono: "bi bi-circle", ruta: "/dashboard/listma", },
    { nombre: "Nuevo", icono: "bi bi-circle", ruta: "/dashboard/addma" },
    ]
  }
];
// *********************************************************************************
//Paciente
const opcionesPaciente = [
  { nombre: "Desarrolladores", icono: "bi bi-grid", ruta: "/dashboard/about", hijos: [], },
  
];



const opcionesInvitado = [
  { nombre: "Acerca de", icono: "bi bi-grid", ruta: "/dashboard/about", hijos: [], },
  { nombre: "Compras", icono: "bi bi-clipboard-data", ruta: "", hijos: [
      { nombre: "Pendientes", icono: "bi bi-circle", ruta: "/dashboard/admuser", },
    ]
  }
];

export { opcionesAdmin,opcionesMedico, opcionesPaciente, opcionesInvitado };