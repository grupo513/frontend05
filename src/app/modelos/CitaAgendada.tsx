import Usuario from "./Usuario";
import Cita from "./Cita";

class CitaAgendada {
  public _id: string;
  public codPaciente: Usuario;
  public codCita: Cita;
  public motivoCita: String;

  constructor( id: string, codp: Usuario,cita:Cita, motivo: String ) {
    this._id = id;
    this.codCita=cita;
    this.codPaciente = codp;
    this.motivoCita = motivo;
  }
}

export default CitaAgendada;