import Perfil from "./Perfil";
import Usuario from "./Usuario";

class Gestion {
  public _id: string;
  public nombreUsuario: string;
  public correoUsuario: string;
  public claveUsuario: string;
  public fechaRegistroUsuario: Date;
  public estadoUsuario: number;
  public nombreImagenUsuario: string;
  public avatarUsuario: string;
  public codPerfil: Perfil;
  public fechaCita: Date;
  public estadoCita: number;
  public codPaciente: Usuario;

  constructor( id: string, nom: string, cor: string, cla: string, fec: Date, est: number, nomi: string, ava: string, codp: Perfil, feci: Date, esta: number, codu: Usuario  ) {
    this._id = id;
    this.nombreUsuario = nom;
    this.correoUsuario = cor;
    this.claveUsuario = cla;
    this.fechaRegistroUsuario = fec;
    this.estadoUsuario = est;
    this.fechaCita = feci;
    this.nombreImagenUsuario = nomi;
    this.avatarUsuario = ava;
    this.codPerfil = codp;
    this.fechaCita = feci;
    this.estadoCita = esta;
    this.codPaciente = codu;
  }
}

export default Gestion;
