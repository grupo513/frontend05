import Usuario from "./Usuario";

class Cita {
  public _id: string;
  public codMedico: Usuario;
  public fechaCita: Date;
  public estadoCita: number;

  constructor( id: string, codm: Usuario, feci: Date, esta: number ) {
    this._id = id;
    this.codMedico = codm;
    this.fechaCita = feci;
    this.estadoCita = esta;
  }
}

export default Cita;