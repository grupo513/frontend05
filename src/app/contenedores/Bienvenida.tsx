export const Bienvenida = () => {
    return (
      <main id="main" className="main">
        <div className="col-lg-11 mx-auto p-4 py-md-5">
          <header className="d-flex align-items-center pb-3 mb-5 border-bottom">
            <i
              className="fa-solid fa-user-doctor fa-2xl"
              style={{ color: "#123399" }}
            ></i>
            <span className="fs-4"> &nbsp; Bienvenido al sistema</span>
          </header>
  
          <main>
            <h1>Consultorio Online</h1>
            <p className="fs-5">
              Es una plataforma web que puede ser usada por médicos, pacientes y administradores, a los médicos les
              permite gestionar citas, de tal forma que son ellos quienes según su disponibilidad  
              horaria crearlas y así posteriormente sean agendadas por los pacientes.  
              <br />
              <small className="text-danger">
                Todo usuario que crea una cuenta se le asignará el perfil de invitado, para modificarlo debe ser autorizado por el Administrador.
              </small>
            </p>
          </main>
        </div>
      </main>
    );
  };